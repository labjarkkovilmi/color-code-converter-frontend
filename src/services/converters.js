import axios from 'axios'
const baseUrl = 'http://localhost:3001'

const getWelcome = async () => {
	const response = await axios.get(baseUrl)
	return response.data
}

const rgbToHex = async (red, green, blue) => {
	console.log(red, green, blue)
	const response = await axios.get(
		`${baseUrl}/rgb-to-hex?r=${red}&g=${green}&b=${blue}`)
	return response.data
}

const hexToRgb = async (red, green, blue) => {
	console.log(red, green, blue)
	const response = await axios.get(
		`${baseUrl}/hex-to-rgb?r=${red}&g=${green}&b=${blue}`)
	return response.data
}

const exportObject = { getWelcome, rgbToHex, hexToRgb }

export default exportObject