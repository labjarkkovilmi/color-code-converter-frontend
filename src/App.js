import './App.css';
import { useState, useEffect } from 'react'
import converterService from './services/converters'

const App = () => {
  const [red, setRed] = useState(255)
  const [green, setGreen] = useState(255)
  const [blue, setBlue] = useState(255)
  const [result, setResult] = useState(["0", "0", "0"])
	const [converter, setConverter] = useState('rgbToHex')

	const borderContainerStyle = {
		backgroundColor: (converter === 'rgbToHex' 
			? `rgb(${red},${green},${blue})`
			: `rgb(${parseInt(red, 16)},${parseInt(green, 16)},${parseInt(blue, 16)})`
		)
	}

	useEffect(() => {
		if (red && green && blue) {
			switch (converter) {
				case 'rgbToHex':
					console.log("RGB conversion")
					converterService.rgbToHex(red, green, blue)
						.then(response => {
							setResult(response.match(/[^#]{1,2}/g))
						})
						.catch(error => console.log(error))
					break
				case 'hexToRgb':
					console.log("HEX conversion")
					converterService.hexToRgb(red, green, blue)
						.then(response => {
							setResult(response.slice(4, response.length-1).split(", "))
						})
						.catch(error => console.log(error))
					break
				default:
					setResult('no value')
				break
			}
		}
		console.log(result)
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [red, green, blue]);

	const onClickHex = () => {
		setConverter('hexToRgb')
		setRed("FF")
		setGreen("FF")
		setBlue("FF")
	}

	const onClickRgb = () => {
		setConverter('rgbToHex')
		setRed("255")
		setGreen("255")
		setBlue("255")
	}

	const converterInput = (inputLength, header) => {
		return (
			<div className="inputContainer">
			<h1>{header}:</h1>
			<input maxLength={{inputLength}} value={red}
				onChange={(event) => setRed(event.target.value)} />
			<input maxLength={{inputLength}} value={green}
				onChange={(event) => setGreen(event.target.value)} />
			<input maxLength={{inputLength}} value={blue}
				onChange={(event) => setBlue(event.target.value)} />
		</div>
		)
	}

	return (
		<div className="borderContainer" style={borderContainerStyle}>
			<div className="container">
				<h1>Color Code Converter</h1>
				{converter === 'rgbToHex' 
					?
					<button onClick={() => onClickHex()}>CONVERTING RGB TO HEX, PRESS TO CHANGE</button>
					: 
					<button onClick={() => onClickRgb()}>CONVERTING HEX TO RGB, PRESS TO CHANGE</button>
				}
				<form >
					<div className="inputContainer">
						{converter === 'hexToRgb' ? converterInput(2, "HEX") : converterInput(3, "RGB")}
					</div>
				</form>
				<div className="output">
					<Result red={red} green={green} blue={blue} result={result} converter={converter} />
				</div>
			</div>
		</div>
  )
}

const Result = ({ red, green, blue, result, converter }) => {
	if (!red || !green || !blue)
		return (
			<h2>Missing values</h2>
		)
	if (converter === 'rgbToHex')
		return (
			<h2>{`HEX: #${result[0]}${result[1]}${result[2]}`}</h2>
		)
	if (converter === 'hexToRgb')
		return (
			<h2>{`RGB: (${result[0]}, ${result[1]}, ${result[2]})`}</h2>
		)
}

export default App
